<?php
 /**
 * Short description for file
 *
 * Long description for file (if any)...
 * User: matt
 * Date: 04/05/15
 * Time: 00:57
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/<CategoryName>
 * @package   mac
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2015 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/mac
 */
include "../Mac.php";

class macTest extends PHPUnit_Framework_TestCase {
    public function setUp()
    {
        //
    }

    public function tearDown()
    {
        //
    }

    public function testMacValidate()
    {
        // test normalization of mac addresses
        $mac=new Actweb\Mac();
        $this->assertEquals('00AABBCCDDEE', $mac->validate('00:aa:Bb:cc:DD:EE'));
        $this->assertEquals('00AABBCCDDEE', $mac->validate('00aaBbccDDEE'));
        $this->assertEquals('00AABBCCDDEE', $mac->validate('00-aa-Bb-cc-DD-EE'));
        $this->assertEquals('00AABBCCDDEE', $mac->validate('00a.aBb.ccD.DEE'));
        // test validation of MAC address
        $this->assertNull($mac->validate('00:aa:Bb:cc:DD:EG'));
        $this->assertNull($mac->validate('00:aa:Bb:cc:DD:'));
    }


}