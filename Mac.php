<?php
/**
 * Normalizes and validates MAC address
 *
 * Normalizes a MAC address by removing any seperating characters and
 * converting all letters to uppercase
 * User: matt
 * Date: 04/05/15
 * Time: 01:08
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/<CategoryName>
 * @package   mac
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2015 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/mac
 */


namespace Actweb;

/**
 * Normalizes and validates a passed MAC address
 *
 * Class Mac
 * User: ${USER}
 * Date: ${DATE}
 * Time: ${TIME}
 *
 * @category  ACTweb/<CategoryName>
 * @package   Mac
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2015 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/${PROJECT_NAME}
 */
class Mac
{
    /**
     * Current MAC address being worked on
     *
     * @var string
     */
    public $mac = '';

    /**
     * Basic Constructor
     */
    function __construct()
    {
        // does nothing just now
    }

    /**
     * This will normalize the supplied mac address and validate it
     * If the MAC is valid, it returns the normalized MAC address
     * else it returns null
     *
     * @param string $mac MAC address to normalize and validate
     *
     * @return mixed|null|string Normalized MAC address or null on invalid MAC
     */
    public function validate($mac)
    {

        /*
         * Convert to uppercase
         */
        $mac = strtoupper($mac);
        /*
         * Strip all non alpha numeric chars
         */
        $mac = preg_replace("/[^A-F0-9]/", '', $mac);
        if (strlen($mac) != 12) {
            $this->mac = null;
            return null;
        }
        /*
         * Set class mac address to this addy
         * This is for future expansion
         */
        $this->mac = $mac;
        return $mac;
    }

}